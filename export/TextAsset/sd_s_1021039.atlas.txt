
sd_s_1021039.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: true
  xy: 395, 252
  size: 55, 117
  orig: 55, 117
  offset: 0, 0
  index: -1
arm_F
  rotate: true
  xy: 269, 201
  size: 58, 116
  orig: 58, 116
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 391, 467
  size: 103, 165
  orig: 103, 165
  offset: 0, 0
  index: -1
brow_1
  rotate: true
  xy: 2, 2
  size: 155, 40
  orig: 155, 40
  offset: 0, 0
  index: -1
brow_2
  rotate: false
  xy: 386, 420
  size: 163, 45
  orig: 163, 45
  offset: 0, 0
  index: -1
brow_3
  rotate: false
  xy: 222, 387
  size: 162, 41
  orig: 162, 41
  offset: 0, 0
  index: -1
bubble_01
  rotate: false
  xy: 643, 747
  size: 178, 131
  orig: 178, 131
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 82, 196
  size: 185, 186
  orig: 185, 186
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 269, 261
  size: 124, 124
  orig: 124, 124
  offset: 0, 0
  index: -1
dead_b
  rotate: true
  xy: 82, 384
  size: 344, 138
  orig: 344, 138
  offset: 0, 0
  index: -1
dead_f
  rotate: false
  xy: 643, 880
  size: 280, 142
  orig: 280, 142
  offset: 0, 0
  index: -1
ear
  rotate: false
  xy: 92, 30
  size: 53, 73
  orig: 53, 73
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 395, 309
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: false
  xy: 595, 574
  size: 153, 171
  orig: 153, 171
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 391, 572
  size: 202, 202
  orig: 202, 202
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 823, 727
  size: 159, 151
  orig: 159, 151
  offset: 0, 0
  index: -1
eye_1
  rotate: true
  xy: 44, 11
  size: 146, 46
  orig: 146, 46
  offset: 0, 0
  index: -1
eye_2
  rotate: false
  xy: 222, 430
  size: 162, 52
  orig: 162, 52
  offset: 0, 0
  index: -1
eye_3
  rotate: true
  xy: 750, 577
  size: 168, 67
  orig: 168, 67
  offset: 0, 0
  index: -1
hair
  rotate: false
  xy: 2, 730
  size: 364, 292
  orig: 364, 292
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 368, 776
  size: 273, 246
  orig: 273, 246
  offset: 0, 0
  index: -1
leg_B
  rotate: false
  xy: 558, 458
  size: 57, 112
  orig: 57, 112
  offset: 0, 0
  index: -1
leg_F
  rotate: true
  xy: 387, 191
  size: 59, 115
  orig: 59, 115
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 925, 886
  size: 77, 34
  orig: 77, 34
  offset: 0, 0
  index: -1
mouth_2
  rotate: false
  xy: 617, 460
  size: 64, 17
  orig: 64, 17
  offset: 0, 0
  index: -1
mouth_3
  rotate: false
  xy: 150, 114
  size: 73, 21
  orig: 73, 21
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 150, 137
  size: 67, 57
  orig: 67, 57
  offset: 0, 0
  index: -1
outerwear_B
  rotate: false
  xy: 819, 585
  size: 158, 140
  orig: 158, 140
  offset: 0, 0
  index: -1
outerwear_F
  rotate: false
  xy: 222, 484
  size: 167, 244
  orig: 167, 244
  offset: 0, 0
  index: -1
uparm_B
  rotate: false
  xy: 92, 105
  size: 56, 89
  orig: 56, 89
  offset: 0, 0
  index: -1
uparm_F
  rotate: false
  xy: 506, 324
  size: 54, 94
  orig: 54, 94
  offset: 0, 0
  index: -1
upleg_B
  rotate: false
  xy: 679, 480
  size: 57, 92
  orig: 57, 92
  offset: 0, 0
  index: -1
upleg_F
  rotate: false
  xy: 617, 479
  size: 60, 93
  orig: 60, 93
  offset: 0, 0
  index: -1
waist
  rotate: false
  xy: 925, 922
  size: 97, 100
  orig: 97, 100
  offset: 0, 0
  index: -1
weapon_01
  rotate: true
  xy: 2, 159
  size: 569, 78
  orig: 569, 78
  offset: 0, 0
  index: -1
