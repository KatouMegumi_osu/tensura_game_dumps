
sd_s_1083104.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: false
  xy: 288, 216
  size: 57, 111
  orig: 57, 111
  offset: 0, 0
  index: -1
arm_F
  rotate: true
  xy: 505, 448
  size: 58, 111
  orig: 58, 111
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 666, 729
  size: 101, 153
  orig: 101, 153
  offset: 0, 0
  index: -1
brow_1
  rotate: false
  xy: 666, 711
  size: 72, 16
  orig: 72, 16
  offset: 0, 0
  index: -1
brow_2
  rotate: true
  xy: 969, 812
  size: 69, 16
  orig: 69, 16
  offset: 0, 0
  index: -1
brow_3
  rotate: false
  xy: 642, 689
  size: 67, 20
  orig: 67, 20
  offset: 0, 0
  index: -1
bubble_01
  rotate: false
  xy: 809, 883
  size: 178, 131
  orig: 178, 131
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 2, 2
  size: 185, 186
  orig: 185, 186
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 162, 204
  size: 124, 124
  orig: 124, 124
  offset: 0, 0
  index: -1
dead_b
  rotate: true
  xy: 162, 330
  size: 276, 137
  orig: 276, 137
  offset: 0, 0
  index: -1
dead_f
  rotate: false
  xy: 562, 884
  size: 245, 130
  orig: 245, 130
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 505, 508
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: false
  xy: 511, 711
  size: 153, 171
  orig: 153, 171
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 301, 417
  size: 202, 202
  orig: 202, 202
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 769, 730
  size: 159, 151
  orig: 159, 151
  offset: 0, 0
  index: -1
eye_1
  rotate: true
  xy: 930, 732
  size: 149, 37
  orig: 149, 37
  offset: 0, 0
  index: -1
eye_2
  rotate: false
  xy: 301, 329
  size: 156, 36
  orig: 156, 36
  offset: 0, 0
  index: -1
eye_3
  rotate: false
  xy: 301, 367
  size: 155, 48
  orig: 155, 48
  offset: 0, 0
  index: -1
hair
  rotate: true
  xy: 295, 621
  size: 275, 214
  orig: 275, 214
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 608
  size: 291, 288
  orig: 291, 288
  offset: 0, 0
  index: -1
leg_B
  rotate: false
  xy: 347, 224
  size: 42, 103
  orig: 42, 103
  offset: 0, 0
  index: -1
leg_F
  rotate: false
  xy: 391, 228
  size: 41, 99
  orig: 41, 99
  offset: 0, 0
  index: -1
mouth_1
  rotate: true
  xy: 989, 949
  size: 65, 32
  orig: 65, 32
  offset: 0, 0
  index: -1
mouth_2
  rotate: true
  xy: 434, 267
  size: 60, 18
  orig: 60, 18
  offset: 0, 0
  index: -1
mouth_3
  rotate: true
  xy: 616, 555
  size: 62, 23
  orig: 62, 23
  offset: 0, 0
  index: -1
nose
  rotate: true
  xy: 505, 390
  size: 56, 61
  orig: 56, 61
  offset: 0, 0
  index: -1
uparm_B
  rotate: false
  xy: 189, 117
  size: 46, 85
  orig: 46, 85
  offset: 0, 0
  index: -1
uparm_F
  rotate: false
  xy: 288, 137
  size: 45, 77
  orig: 45, 77
  offset: 0, 0
  index: -1
upleg_B
  rotate: false
  xy: 237, 123
  size: 49, 79
  orig: 49, 79
  offset: 0, 0
  index: -1
upleg_F
  rotate: false
  xy: 189, 45
  size: 45, 70
  orig: 45, 70
  offset: 0, 0
  index: -1
waist
  rotate: false
  xy: 511, 619
  size: 129, 90
  orig: 129, 90
  offset: 0, 0
  index: -1
waist_F_01
  rotate: false
  xy: 189, 15
  size: 31, 28
  orig: 31, 28
  offset: 0, 0
  index: -1
weapon_01
  rotate: true
  xy: 2, 190
  size: 416, 158
  orig: 416, 158
  offset: 0, 0
  index: -1
weapon_02
  rotate: false
  xy: 2, 898
  size: 558, 116
  orig: 558, 116
  offset: 0, 0
  index: -1
