
sd_s_1025046.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: true
  xy: 798, 671
  size: 39, 88
  orig: 39, 88
  offset: 0, 0
  index: -1
arm_F
  rotate: true
  xy: 928, 792
  size: 41, 93
  orig: 41, 93
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 484, 11
  size: 71, 90
  orig: 71, 90
  offset: 0, 0
  index: -1
bubble_01
  rotate: false
  xy: 523, 331
  size: 125, 92
  orig: 125, 92
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 233, 414
  size: 130, 130
  orig: 130, 130
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 395, 14
  size: 87, 87
  orig: 87, 87
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 734, 897
  size: 172, 125
  orig: 172, 125
  offset: 0, 0
  index: -1
dead_f
  rotate: true
  xy: 165, 102
  size: 170, 116
  orig: 170, 116
  offset: 0, 0
  index: -1
ef_circle_A
  rotate: false
  xy: 426, 103
  size: 102, 102
  orig: 102, 102
  offset: 0, 0
  index: -1
ef_circle_B
  rotate: false
  xy: 524, 225
  size: 104, 104
  orig: 104, 104
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 908, 714
  size: 76, 76
  orig: 76, 76
  offset: 0, 0
  index: -1
ef_light_common_A
  rotate: false
  xy: 801, 634
  size: 35, 35
  orig: 35, 35
  offset: 0, 0
  index: -1
ef_light_common_B
  rotate: true
  xy: 986, 689
  size: 35, 36
  orig: 35, 36
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: true
  xy: 2, 2
  size: 107, 120
  orig: 107, 120
  offset: 0, 0
  index: -1
ef_particle_unatt_002b
  rotate: false
  xy: 2, 274
  size: 223, 208
  orig: 223, 208
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 233, 546
  size: 141, 141
  orig: 141, 141
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 365, 425
  size: 111, 106
  orig: 111, 106
  offset: 0, 0
  index: -1
eye_1
  rotate: true
  xy: 607, 5
  size: 108, 31
  orig: 108, 31
  offset: 0, 0
  index: -1
eye_2
  rotate: true
  xy: 630, 212
  size: 117, 25
  orig: 117, 25
  offset: 0, 0
  index: -1
eye_3
  rotate: false
  xy: 678, 668
  size: 118, 42
  orig: 118, 42
  offset: 0, 0
  index: -1
fire_circle
  rotate: false
  xy: 2, 111
  size: 161, 161
  orig: 161, 161
  offset: 0, 0
  index: -1
fire_pillar1
  rotate: true
  xy: 308, 2
  size: 104, 85
  orig: 104, 85
  offset: 0, 0
  index: -1
fire_pillar2
  rotate: true
  xy: 734, 796
  size: 99, 192
  orig: 99, 192
  offset: 0, 0
  index: -1
fire_pillar3
  rotate: false
  xy: 928, 835
  size: 94, 187
  orig: 94, 187
  offset: 0, 0
  index: -1
fire_pillar4
  rotate: true
  xy: 124, 8
  size: 92, 182
  orig: 92, 182
  offset: 0, 0
  index: -1
fire_pillar5
  rotate: true
  xy: 300, 689
  size: 96, 193
  orig: 96, 193
  offset: 0, 0
  index: -1
fire_pillar6
  rotate: true
  xy: 525, 731
  size: 85, 193
  orig: 85, 193
  offset: 0, 0
  index: -1
fire_pillar7
  rotate: true
  xy: 720, 712
  size: 82, 186
  orig: 82, 186
  offset: 0, 0
  index: -1
fire_pillar8
  rotate: false
  xy: 351, 158
  size: 73, 150
  orig: 73, 150
  offset: 0, 0
  index: -1
flame_1
  rotate: true
  xy: 478, 425
  size: 75, 164
  orig: 75, 164
  offset: 0, 0
  index: -1
flame_2
  rotate: true
  xy: 490, 574
  size: 77, 174
  orig: 77, 174
  offset: 0, 0
  index: -1
flame_3
  rotate: true
  xy: 495, 653
  size: 76, 181
  orig: 76, 181
  offset: 0, 0
  index: -1
flame_4
  rotate: false
  xy: 283, 148
  size: 66, 160
  orig: 66, 160
  offset: 0, 0
  index: -1
flame_5
  rotate: true
  xy: 490, 502
  size: 70, 165
  orig: 70, 165
  offset: 0, 0
  index: -1
flame_aura
  rotate: false
  xy: 376, 533
  size: 112, 154
  orig: 112, 154
  offset: 0, 0
  index: -1
flame_aura_A
  rotate: true
  xy: 227, 310
  size: 102, 146
  orig: 102, 146
  offset: 0, 0
  index: -1
flame_aura_B
  rotate: true
  xy: 375, 321
  size: 102, 146
  orig: 102, 146
  offset: 0, 0
  index: -1
hair_B
  rotate: false
  xy: 2, 715
  size: 296, 307
  orig: 296, 307
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 525, 818
  size: 207, 204
  orig: 207, 204
  offset: 0, 0
  index: -1
impact_A
  rotate: true
  xy: 557, 7
  size: 106, 48
  orig: 106, 48
  offset: 0, 0
  index: -1
leg_B
  rotate: false
  xy: 628, 132
  size: 42, 78
  orig: 42, 78
  offset: 0, 0
  index: -1
leg_F
  rotate: false
  xy: 644, 431
  size: 41, 68
  orig: 41, 68
  offset: 0, 0
  index: -1
magic_team
  rotate: false
  xy: 2, 484
  size: 229, 229
  orig: 229, 229
  offset: 0, 0
  index: -1
mask
  rotate: false
  xy: 300, 787
  size: 223, 235
  orig: 223, 235
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 233, 691
  size: 37, 22
  orig: 37, 22
  offset: 0, 0
  index: -1
mouth_2
  rotate: true
  xy: 908, 990
  size: 32, 12
  orig: 32, 12
  offset: 0, 0
  index: -1
mouth_3
  rotate: true
  xy: 272, 689
  size: 24, 11
  orig: 24, 11
  offset: 0, 0
  index: -1
tail
  rotate: true
  xy: 426, 207
  size: 112, 96
  orig: 112, 96
  offset: 0, 0
  index: -1
trajectory_A
  rotate: false
  xy: 678, 616
  size: 74, 50
  orig: 74, 50
  offset: 0, 0
  index: -1
uparm_B
  rotate: true
  xy: 227, 276
  size: 32, 53
  orig: 32, 53
  offset: 0, 0
  index: -1
uparm_F
  rotate: false
  xy: 986, 726
  size: 36, 64
  orig: 36, 64
  offset: 0, 0
  index: -1
upleg_B
  rotate: true
  xy: 666, 569
  size: 45, 69
  orig: 45, 69
  offset: 0, 0
  index: -1
upleg_F
  rotate: true
  xy: 351, 108
  size: 48, 73
  orig: 48, 73
  offset: 0, 0
  index: -1
waist
  rotate: false
  xy: 657, 501
  size: 69, 66
  orig: 69, 66
  offset: 0, 0
  index: -1
waist_acc_F1
  rotate: false
  xy: 754, 616
  size: 45, 50
  orig: 45, 50
  offset: 0, 0
  index: -1
waist_acc_F2
  rotate: false
  xy: 283, 114
  size: 60, 32
  orig: 60, 32
  offset: 0, 0
  index: -1
waist_acc_F3
  rotate: false
  xy: 495, 732
  size: 27, 53
  orig: 27, 53
  offset: 0, 0
  index: -1
waist_cloth1
  rotate: true
  xy: 687, 432
  size: 67, 41
  orig: 67, 41
  offset: 0, 0
  index: -1
waist_cloth2
  rotate: false
  xy: 530, 115
  size: 96, 108
  orig: 96, 108
  offset: 0, 0
  index: -1
weast_acc_B
  rotate: true
  xy: 737, 581
  size: 33, 52
  orig: 33, 52
  offset: 0, 0
  index: -1
white
  rotate: false
  xy: 720, 805
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
