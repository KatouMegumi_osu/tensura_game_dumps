
sd_s_1008011.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: true
  xy: 2, 20
  size: 41, 84
  orig: 41, 84
  offset: 0, 0
  index: -1
arm_F
  rotate: false
  xy: 496, 38
  size: 42, 83
  orig: 42, 83
  offset: 0, 0
  index: -1
backhair
  rotate: false
  xy: 88, 420
  size: 148, 320
  orig: 148, 320
  offset: 0, 0
  index: -1
bangs
  rotate: false
  xy: 284, 792
  size: 244, 230
  orig: 244, 230
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 338, 21
  size: 85, 118
  orig: 85, 118
  offset: 0, 0
  index: -1
brow_1
  rotate: false
  xy: 530, 793
  size: 86, 19
  orig: 86, 19
  offset: 0, 0
  index: -1
brow_2
  rotate: true
  xy: 1007, 945
  size: 77, 15
  orig: 77, 15
  offset: 0, 0
  index: -1
brow_3
  rotate: true
  xy: 593, 621
  size: 75, 25
  orig: 75, 25
  offset: 0, 0
  index: -1
bubble_01
  rotate: true
  xy: 232, 108
  size: 125, 92
  orig: 125, 92
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 482, 344
  size: 130, 130
  orig: 130, 130
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 430, 123
  size: 87, 87
  orig: 87, 87
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 284, 698
  size: 242, 92
  orig: 242, 92
  offset: 0, 0
  index: -1
dead_f
  rotate: true
  xy: 269, 289
  size: 199, 97
  orig: 199, 97
  offset: 0, 0
  index: -1
drak_aura
  rotate: false
  xy: 368, 320
  size: 112, 154
  orig: 112, 154
  offset: 0, 0
  index: -1
ef_circle_A
  rotate: false
  xy: 326, 108
  size: 102, 102
  orig: 102, 102
  offset: 0, 0
  index: -1
ef_circle_B
  rotate: false
  xy: 232, 2
  size: 104, 104
  orig: 104, 104
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 540, 49
  size: 76, 76
  orig: 76, 76
  offset: 0, 0
  index: -1
ef_kira_A
  rotate: true
  xy: 956, 784
  size: 64, 65
  orig: 64, 65
  offset: 0, 0
  index: -1
ef_light_common_A
  rotate: false
  xy: 591, 12
  size: 35, 35
  orig: 35, 35
  offset: 0, 0
  index: -1
ef_light_common_B
  rotate: false
  xy: 238, 704
  size: 35, 36
  orig: 35, 36
  offset: 0, 0
  index: -1
ef_move_A
  rotate: true
  xy: 2, 63
  size: 276, 85
  orig: 276, 85
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: true
  xy: 482, 235
  size: 107, 120
  orig: 107, 120
  offset: 0, 0
  index: -1
ef_particle_unatt_002b
  rotate: false
  xy: 530, 814
  size: 223, 208
  orig: 223, 208
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 89, 92
  size: 141, 141
  orig: 141, 141
  offset: 0, 0
  index: -1
ef_slash_unatt_001
  rotate: false
  xy: 2, 742
  size: 280, 280
  orig: 280, 280
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 368, 212
  size: 111, 106
  orig: 111, 106
  offset: 0, 0
  index: -1
eye_1
  rotate: true
  xy: 458, 21
  size: 100, 36
  orig: 100, 36
  offset: 0, 0
  index: -1
eye_2
  rotate: false
  xy: 89, 65
  size: 130, 25
  orig: 130, 25
  offset: 0, 0
  index: -1
eye_3
  rotate: true
  xy: 956, 888
  size: 134, 49
  orig: 134, 49
  offset: 0, 0
  index: -1
hair_acc
  rotate: false
  xy: 540, 2
  size: 49, 45
  orig: 49, 45
  offset: 0, 0
  index: -1
haze
  rotate: true
  xy: 433, 476
  size: 220, 158
  orig: 220, 158
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 238, 490
  size: 193, 206
  orig: 193, 206
  offset: 0, 0
  index: -1
horn
  rotate: true
  xy: 269, 241
  size: 46, 88
  orig: 46, 88
  offset: 0, 0
  index: -1
leg_B
  rotate: true
  xy: 618, 783
  size: 29, 76
  orig: 29, 76
  offset: 0, 0
  index: -1
leg_F
  rotate: true
  xy: 696, 779
  size: 33, 74
  orig: 33, 74
  offset: 0, 0
  index: -1
mouth_1
  rotate: true
  xy: 496, 2
  size: 34, 22
  orig: 34, 22
  offset: 0, 0
  index: -1
mouth_2
  rotate: false
  xy: 591, 2
  size: 30, 8
  orig: 30, 8
  offset: 0, 0
  index: -1
mouth_3
  rotate: false
  xy: 481, 212
  size: 33, 21
  orig: 33, 21
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 846, 755
  size: 67, 60
  orig: 67, 60
  offset: 0, 0
  index: -1
shock_wave
  rotate: true
  xy: 89, 235
  size: 183, 178
  orig: 183, 178
  offset: 0, 0
  index: -1
sword_ef
  rotate: true
  xy: 519, 127
  size: 106, 74
  orig: 106, 74
  offset: 0, 0
  index: -1
sword_trajectory
  rotate: false
  xy: 755, 817
  size: 199, 205
  orig: 199, 205
  offset: 0, 0
  index: -1
uparm_B
  rotate: true
  xy: 956, 749
  size: 33, 58
  orig: 33, 58
  offset: 0, 0
  index: -1
uparm_F
  rotate: true
  xy: 956, 850
  size: 36, 66
  orig: 36, 66
  offset: 0, 0
  index: -1
upleg_B
  rotate: false
  xy: 915, 744
  size: 39, 71
  orig: 39, 71
  offset: 0, 0
  index: -1
upleg_F
  rotate: false
  xy: 569, 698
  size: 38, 71
  orig: 38, 71
  offset: 0, 0
  index: -1
waist_B
  rotate: true
  xy: 528, 698
  size: 71, 39
  orig: 71, 39
  offset: 0, 0
  index: -1
waist_F
  rotate: false
  xy: 772, 771
  size: 72, 44
  orig: 72, 44
  offset: 0, 0
  index: -1
weapon_01
  rotate: true
  xy: 2, 341
  size: 399, 84
  orig: 399, 84
  offset: 0, 0
  index: -1
white
  rotate: false
  xy: 238, 477
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
