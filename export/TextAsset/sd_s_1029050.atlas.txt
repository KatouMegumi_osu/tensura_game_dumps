
sd_s_1029050.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: true
  xy: 653, 583
  size: 55, 120
  orig: 55, 120
  offset: 0, 0
  index: -1
arm_F
  rotate: true
  xy: 310, 556
  size: 58, 119
  orig: 58, 119
  offset: 0, 0
  index: -1
backhair
  rotate: false
  xy: 2, 591
  size: 306, 324
  orig: 306, 324
  offset: 0, 0
  index: -1
bangs
  rotate: false
  xy: 2, 306
  size: 299, 283
  orig: 299, 283
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 799, 574
  size: 99, 169
  orig: 99, 169
  offset: 0, 0
  index: -1
brow_1
  rotate: true
  xy: 511, 833
  size: 82, 28
  orig: 82, 28
  offset: 0, 0
  index: -1
brow_2
  rotate: false
  xy: 2, 2
  size: 99, 15
  orig: 99, 15
  offset: 0, 0
  index: -1
brow_3
  rotate: true
  xy: 970, 576
  size: 97, 35
  orig: 97, 35
  offset: 0, 0
  index: -1
bubble_01
  rotate: true
  xy: 666, 640
  size: 178, 131
  orig: 178, 131
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 762, 836
  size: 185, 186
  orig: 185, 186
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 283, 33
  size: 124, 124
  orig: 124, 124
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 2, 159
  size: 311, 145
  orig: 311, 145
  offset: 0, 0
  index: -1
dead_f
  rotate: false
  xy: 2, 19
  size: 279, 138
  orig: 279, 138
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 303, 445
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: false
  xy: 511, 647
  size: 153, 171
  orig: 153, 171
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 558, 820
  size: 202, 202
  orig: 202, 202
  offset: 0, 0
  index: -1
effect_bow
  rotate: true
  xy: 799, 675
  size: 159, 151
  orig: 159, 151
  offset: 0, 0
  index: -1
eye_1
  rotate: false
  xy: 511, 597
  size: 140, 48
  orig: 140, 48
  offset: 0, 0
  index: -1
eye_2
  rotate: true
  xy: 952, 675
  size: 169, 44
  orig: 169, 44
  offset: 0, 0
  index: -1
eye_3
  rotate: true
  xy: 949, 846
  size: 176, 63
  orig: 176, 63
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 310, 616
  size: 299, 199
  orig: 299, 199
  offset: 0, 0
  index: -1
leg_B
  rotate: false
  xy: 362, 214
  size: 43, 109
  orig: 43, 109
  offset: 0, 0
  index: -1
leg_F
  rotate: false
  xy: 315, 208
  size: 45, 115
  orig: 45, 115
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 570, 558
  size: 51, 37
  orig: 51, 37
  offset: 0, 0
  index: -1
mouth_2
  rotate: false
  xy: 762, 824
  size: 30, 10
  orig: 30, 10
  offset: 0, 0
  index: -1
mouth_3
  rotate: true
  xy: 998, 795
  size: 49, 24
  orig: 49, 24
  offset: 0, 0
  index: -1
uparm_B
  rotate: false
  xy: 431, 527
  size: 49, 87
  orig: 49, 87
  offset: 0, 0
  index: -1
uparm_F
  rotate: false
  xy: 391, 351
  size: 48, 92
  orig: 48, 92
  offset: 0, 0
  index: -1
upleg_B
  rotate: true
  xy: 482, 497
  size: 48, 84
  orig: 48, 84
  offset: 0, 0
  index: -1
upleg_F
  rotate: true
  xy: 482, 547
  size: 48, 86
  orig: 48, 86
  offset: 0, 0
  index: -1
waist
  rotate: true
  xy: 303, 325
  size: 118, 86
  orig: 118, 86
  offset: 0, 0
  index: -1
weapon_01
  rotate: false
  xy: 2, 917
  size: 554, 105
  orig: 554, 105
  offset: 0, 0
  index: -1
