
sd_s_1053074.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
body_B
  rotate: false
  xy: 609, 785
  size: 204, 189
  orig: 204, 189
  offset: 0, 0
  index: -1
body_F
  rotate: false
  xy: 2, 344
  size: 287, 286
  orig: 287, 286
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 344, 808
  size: 263, 166
  orig: 263, 166
  offset: 0, 0
  index: -1
dead_f
  rotate: false
  xy: 344, 653
  size: 250, 153
  orig: 250, 153
  offset: 0, 0
  index: -1
ear_B
  rotate: false
  xy: 520, 503
  size: 126, 148
  orig: 126, 148
  offset: 0, 0
  index: -1
ear_F
  rotate: false
  xy: 299, 195
  size: 125, 147
  orig: 125, 147
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 291, 344
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: false
  xy: 815, 803
  size: 153, 171
  orig: 153, 171
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 2, 140
  size: 202, 202
  orig: 202, 202
  offset: 0, 0
  index: -1
eye_1
  rotate: true
  xy: 291, 455
  size: 175, 87
  orig: 175, 87
  offset: 0, 0
  index: -1
eye_2
  rotate: false
  xy: 648, 470
  size: 171, 80
  orig: 171, 80
  offset: 0, 0
  index: -1
eye_3
  rotate: true
  xy: 206, 162
  size: 180, 91
  orig: 180, 91
  offset: 0, 0
  index: -1
foot_back_B_1
  rotate: false
  xy: 380, 505
  size: 138, 146
  orig: 138, 146
  offset: 0, 0
  index: -1
foot_back_B_2
  rotate: false
  xy: 472, 359
  size: 63, 142
  orig: 63, 142
  offset: 0, 0
  index: -1
foot_back_F_1
  rotate: true
  xy: 825, 662
  size: 139, 173
  orig: 139, 173
  offset: 0, 0
  index: -1
foot_back_F_2
  rotate: false
  xy: 537, 371
  size: 68, 130
  orig: 68, 130
  offset: 0, 0
  index: -1
foot_front_ B_1
  rotate: true
  xy: 821, 427
  size: 116, 162
  orig: 116, 162
  offset: 0, 0
  index: -1
foot_front_ B_2
  rotate: false
  xy: 402, 348
  size: 68, 155
  orig: 68, 155
  offset: 0, 0
  index: -1
foot_front_F_1
  rotate: true
  xy: 825, 545
  size: 115, 165
  orig: 115, 165
  offset: 0, 0
  index: -1
foot_front_F_2
  rotate: false
  xy: 426, 185
  size: 74, 161
  orig: 74, 161
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 632
  size: 340, 342
  orig: 340, 342
  offset: 0, 0
  index: -1
mouth_under
  rotate: false
  xy: 648, 552
  size: 175, 108
  orig: 175, 108
  offset: 0, 0
  index: -1
mouth_upper
  rotate: false
  xy: 2, 2
  size: 197, 136
  orig: 197, 136
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 596, 662
  size: 227, 121
  orig: 227, 121
  offset: 0, 0
  index: -1
