
sd_u_1008014.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: true
  xy: 478, 254
  size: 43, 87
  orig: 43, 87
  offset: 0, 0
  index: -1
arm_F
  rotate: false
  xy: 432, 271
  size: 44, 87
  orig: 44, 87
  offset: 0, 0
  index: -1
backhair
  rotate: true
  xy: 285, 786
  size: 148, 320
  orig: 148, 320
  offset: 0, 0
  index: -1
bangs
  rotate: false
  xy: 2, 127
  size: 244, 230
  orig: 244, 230
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 654, 319
  size: 83, 86
  orig: 83, 86
  offset: 0, 0
  index: -1
brow_1
  rotate: false
  xy: 2, 5
  size: 86, 19
  orig: 86, 19
  offset: 0, 0
  index: -1
brow_2
  rotate: false
  xy: 889, 322
  size: 77, 15
  orig: 77, 15
  offset: 0, 0
  index: -1
brow_3
  rotate: false
  xy: 401, 360
  size: 75, 25
  orig: 75, 25
  offset: 0, 0
  index: -1
bubble_01
  rotate: false
  xy: 665, 597
  size: 125, 92
  orig: 125, 92
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 757, 322
  size: 130, 130
  orig: 130, 130
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 478, 299
  size: 87, 87
  orig: 87, 87
  offset: 0, 0
  index: -1
candy
  rotate: false
  xy: 498, 86
  size: 28, 46
  orig: 28, 46
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 2, 26
  size: 242, 99
  orig: 242, 99
  offset: 0, 0
  index: -1
dead_f
  rotate: true
  xy: 464, 388
  size: 205, 99
  orig: 205, 99
  offset: 0, 0
  index: -1
decoration_1
  rotate: false
  xy: 972, 598
  size: 30, 34
  orig: 30, 34
  offset: 0, 0
  index: -1
decoration_2
  rotate: true
  xy: 610, 130
  size: 30, 34
  orig: 30, 34
  offset: 0, 0
  index: -1
decoration_3
  rotate: false
  xy: 991, 402
  size: 30, 34
  orig: 30, 34
  offset: 0, 0
  index: -1
ef_fire_3
  rotate: false
  xy: 909, 865
  size: 113, 155
  orig: 113, 155
  offset: 0, 0
  index: -1
ef_fire_5
  rotate: false
  xy: 686, 860
  size: 221, 160
  orig: 221, 160
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 462, 176
  size: 76, 76
  orig: 76, 76
  offset: 0, 0
  index: -1
ef_grain_unatt_001
  rotate: true
  xy: 359, 148
  size: 102, 101
  orig: 102, 101
  offset: 0, 0
  index: -1
ef_grain_unatt_003
  rotate: false
  xy: 248, 135
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
ef_grain_unatt_007
  rotate: false
  xy: 846, 717
  size: 141, 141
  orig: 141, 141
  offset: 0, 0
  index: -1
ef_grd_001
  rotate: false
  xy: 658, 895
  size: 22, 39
  orig: 22, 39
  offset: 0, 0
  index: -1
ef_kira_B
  rotate: false
  xy: 567, 317
  size: 85, 85
  orig: 85, 85
  offset: 0, 0
  index: -1
ef_linework_A2
  rotate: true
  xy: 285, 595
  size: 189, 260
  orig: 189, 260
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: false
  xy: 900, 476
  size: 107, 120
  orig: 107, 120
  offset: 0, 0
  index: -1
ef_particle_unatt_002c
  rotate: false
  xy: 665, 691
  size: 179, 167
  orig: 179, 167
  offset: 0, 0
  index: -1
ef_particle_unatt_kari
  rotate: false
  xy: 846, 598
  size: 124, 117
  orig: 124, 117
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 757, 454
  size: 141, 141
  orig: 141, 141
  offset: 0, 0
  index: -1
ef_ring
  rotate: false
  xy: 565, 404
  size: 190, 190
  orig: 190, 190
  offset: 0, 0
  index: -1
ef_slash_unatt_001
  rotate: true
  xy: 547, 596
  size: 188, 116
  orig: 188, 116
  offset: 0, 0
  index: -1
ef_sword
  rotate: true
  xy: 356, 252
  size: 106, 74
  orig: 106, 74
  offset: 0, 0
  index: -1
effect_bow
  rotate: true
  xy: 248, 246
  size: 111, 106
  orig: 111, 106
  offset: 0, 0
  index: -1
eye_1
  rotate: false
  xy: 900, 438
  size: 100, 36
  orig: 100, 36
  offset: 0, 0
  index: -1
eye_2
  rotate: false
  xy: 269, 360
  size: 130, 25
  orig: 130, 25
  offset: 0, 0
  index: -1
eye_3
  rotate: true
  xy: 607, 800
  size: 134, 49
  orig: 134, 49
  offset: 0, 0
  index: -1
fire_powder_A
  rotate: true
  xy: 567, 241
  size: 74, 78
  orig: 74, 78
  offset: 0, 0
  index: -1
hair_acc
  rotate: false
  xy: 359, 83
  size: 68, 63
  orig: 68, 63
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 269, 387
  size: 193, 206
  orig: 193, 206
  offset: 0, 0
  index: -1
horn
  rotate: false
  xy: 792, 601
  size: 46, 88
  orig: 46, 88
  offset: 0, 0
  index: -1
leg_B
  rotate: true
  xy: 538, 131
  size: 32, 70
  orig: 32, 70
  offset: 0, 0
  index: -1
leg_F
  rotate: false
  xy: 989, 720
  size: 29, 74
  orig: 29, 74
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 90, 2
  size: 34, 22
  orig: 34, 22
  offset: 0, 0
  index: -1
mouth_2
  rotate: true
  xy: 269, 651
  size: 30, 8
  orig: 30, 8
  offset: 0, 0
  index: -1
mouth_3
  rotate: true
  xy: 658, 860
  size: 33, 21
  orig: 33, 21
  offset: 0, 0
  index: -1
tree
  rotate: false
  xy: 2, 359
  size: 265, 322
  orig: 265, 322
  offset: 0, 0
  index: -1
tree_blur
  rotate: false
  xy: 2, 683
  size: 281, 337
  orig: 281, 337
  offset: 0, 0
  index: -1
treestar
  rotate: false
  xy: 528, 72
  size: 59, 57
  orig: 59, 57
  offset: 0, 0
  index: -1
treestar_blur
  rotate: false
  xy: 540, 165
  size: 76, 74
  orig: 76, 74
  offset: 0, 0
  index: -1
uparm_B
  rotate: false
  xy: 618, 162
  size: 36, 77
  orig: 36, 77
  offset: 0, 0
  index: -1
uparm_F
  rotate: false
  xy: 989, 796
  size: 33, 67
  orig: 33, 67
  offset: 0, 0
  index: -1
upleg_B
  rotate: true
  xy: 462, 134
  size: 40, 74
  orig: 40, 74
  offset: 0, 0
  index: -1
upleg_F
  rotate: false
  xy: 972, 634
  size: 39, 81
  orig: 39, 81
  offset: 0, 0
  index: -1
waist
  rotate: false
  xy: 429, 81
  size: 67, 51
  orig: 67, 51
  offset: 0, 0
  index: -1
waist_F
  rotate: false
  xy: 889, 339
  size: 100, 97
  orig: 100, 97
  offset: 0, 0
  index: -1
weapon_01
  rotate: false
  xy: 285, 936
  size: 399, 84
  orig: 399, 84
  offset: 0, 0
  index: -1
white
  rotate: false
  xy: 540, 241
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
