
sd_u_1004008.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: true
  xy: 846, 461
  size: 41, 72
  orig: 41, 72
  offset: 0, 0
  index: -1
arm_F
  rotate: false
  xy: 868, 913
  size: 45, 72
  orig: 45, 72
  offset: 0, 0
  index: -1
bd_B
  rotate: false
  xy: 670, 220
  size: 46, 40
  orig: 46, 40
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 471, 121
  size: 59, 74
  orig: 59, 74
  offset: 0, 0
  index: -1
body_acc
  rotate: true
  xy: 326, 692
  size: 59, 25
  orig: 59, 25
  offset: 0, 0
  index: -1
brow_1
  rotate: false
  xy: 606, 110
  size: 59, 20
  orig: 59, 20
  offset: 0, 0
  index: -1
brow_2
  rotate: false
  xy: 674, 262
  size: 59, 14
  orig: 59, 14
  offset: 0, 0
  index: -1
brow_3
  rotate: false
  xy: 959, 322
  size: 56, 20
  orig: 56, 20
  offset: 0, 0
  index: -1
bubble_01
  rotate: false
  xy: 915, 943
  size: 107, 79
  orig: 107, 79
  offset: 0, 0
  index: -1
bubble_02
  rotate: true
  xy: 356, 7
  size: 111, 112
  orig: 111, 112
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 471, 182
  size: 74, 74
  orig: 74, 74
  offset: 0, 0
  index: -1
dead_b
  rotate: true
  xy: 510, 530
  size: 206, 94
  orig: 206, 94
  offset: 0, 0
  index: -1
dead_f
  rotate: false
  xy: 698, 891
  size: 168, 94
  orig: 168, 94
  offset: 0, 0
  index: -1
ef_aura_A
  rotate: false
  xy: 244, 455
  size: 108, 235
  orig: 108, 235
  offset: 0, 0
  index: -1
ef_aura_B
  rotate: false
  xy: 2, 240
  size: 189, 223
  orig: 189, 223
  offset: 0, 0
  index: -1
ef_explosion_A
  rotate: false
  xy: 326, 753
  size: 202, 202
  orig: 202, 202
  offset: 0, 0
  index: -1
ef_fire_A001
  rotate: false
  xy: 959, 344
  size: 56, 109
  orig: 56, 109
  offset: 0, 0
  index: -1
ef_fire_A002
  rotate: false
  xy: 547, 165
  size: 55, 91
  orig: 55, 91
  offset: 0, 0
  index: -1
ef_fire_A003
  rotate: true
  xy: 764, 266
  size: 55, 68
  orig: 55, 68
  offset: 0, 0
  index: -1
ef_fire_B001
  rotate: true
  xy: 266, 4
  size: 37, 62
  orig: 37, 62
  offset: 0, 0
  index: -1
ef_fire_B002
  rotate: true
  xy: 606, 132
  size: 41, 64
  orig: 41, 64
  offset: 0, 0
  index: -1
ef_fire_B003
  rotate: false
  xy: 946, 273
  size: 40, 47
  orig: 40, 47
  offset: 0, 0
  index: -1
ef_fire_C
  rotate: false
  xy: 988, 267
  size: 31, 53
  orig: 31, 53
  offset: 0, 0
  index: -1
ef_fire_D
  rotate: false
  xy: 676, 361
  size: 25, 35
  orig: 25, 35
  offset: 0, 0
  index: -1
ef_fire_E_001
  rotate: false
  xy: 530, 738
  size: 156, 217
  orig: 156, 217
  offset: 0, 0
  index: -1
ef_fire_E_002
  rotate: false
  xy: 193, 236
  size: 156, 217
  orig: 156, 217
  offset: 0, 0
  index: -1
ef_fire_E_003
  rotate: false
  xy: 354, 530
  size: 154, 221
  orig: 154, 221
  offset: 0, 0
  index: -1
ef_fog_A
  rotate: false
  xy: 929, 455
  size: 86, 110
  orig: 86, 110
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 834, 258
  size: 65, 65
  orig: 65, 65
  offset: 0, 0
  index: -1
ef_grain_fire_002
  rotate: false
  xy: 474, 361
  size: 95, 95
  orig: 95, 95
  offset: 0, 0
  index: -1
ef_grain_unatt_001
  rotate: false
  xy: 474, 258
  size: 101, 101
  orig: 101, 101
  offset: 0, 0
  index: -1
ef_grain_unatt_003
  rotate: false
  xy: 705, 323
  size: 127, 127
  orig: 127, 127
  offset: 0, 0
  index: -1
ef_grain_unatt_004
  rotate: false
  xy: 705, 452
  size: 139, 139
  orig: 139, 139
  offset: 0, 0
  index: -1
ef_grd_A
  rotate: false
  xy: 674, 334
  size: 14, 25
  orig: 14, 25
  offset: 0, 0
  index: -1
ef_jagged_D
  rotate: true
  xy: 606, 543
  size: 193, 97
  orig: 193, 97
  offset: 0, 0
  index: -1
ef_light_common_A
  rotate: false
  xy: 500, 89
  size: 30, 30
  orig: 30, 30
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: true
  xy: 351, 238
  size: 92, 102
  orig: 92, 102
  offset: 0, 0
  index: -1
ef_particle_unatt_002b
  rotate: true
  xy: 176, 43
  size: 191, 178
  orig: 191, 178
  offset: 0, 0
  index: -1
ef_puddle_A
  rotate: true
  xy: 834, 325
  size: 125, 123
  orig: 125, 123
  offset: 0, 0
  index: -1
ef_puddle_B
  rotate: false
  xy: 571, 398
  size: 132, 130
  orig: 132, 130
  offset: 0, 0
  index: -1
ef_radical_A
  rotate: true
  xy: 354, 458
  size: 70, 215
  orig: 70, 215
  offset: 0, 0
  index: -1
ef_radical_B
  rotate: true
  xy: 698, 987
  size: 35, 215
  orig: 35, 215
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 351, 332
  size: 121, 121
  orig: 121, 121
  offset: 0, 0
  index: -1
ef_sisimai
  rotate: false
  xy: 2, 692
  size: 322, 330
  orig: 322, 330
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 577, 268
  size: 95, 91
  orig: 95, 91
  offset: 0, 0
  index: -1
eye_1
  rotate: false
  xy: 176, 9
  size: 88, 32
  orig: 88, 32
  offset: 0, 0
  index: -1
eye_2
  rotate: false
  xy: 69, 7
  size: 100, 18
  orig: 100, 18
  offset: 0, 0
  index: -1
eye_3
  rotate: false
  xy: 571, 361
  size: 103, 35
  orig: 103, 35
  offset: 0, 0
  index: -1
fire_powder_A
  rotate: false
  xy: 604, 199
  size: 64, 67
  orig: 64, 67
  offset: 0, 0
  index: -1
flame_aura_A
  rotate: false
  xy: 926, 696
  size: 88, 125
  orig: 88, 125
  offset: 0, 0
  index: -1
hair_B
  rotate: false
  xy: 2, 465
  size: 240, 225
  orig: 240, 225
  offset: 0, 0
  index: -1
hair_F
  rotate: true
  xy: 2, 27
  size: 211, 172
  orig: 211, 172
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 705, 593
  size: 149, 136
  orig: 149, 136
  offset: 0, 0
  index: -1
impact_ef
  rotate: false
  xy: 688, 744
  size: 149, 145
  orig: 149, 145
  offset: 0, 0
  index: -1
leg_B
  rotate: true
  xy: 604, 175
  size: 22, 64
  orig: 22, 64
  offset: 0, 0
  index: -1
leg_F
  rotate: true
  xy: 2, 2
  size: 23, 65
  orig: 23, 65
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 868, 893
  size: 28, 18
  orig: 28, 18
  offset: 0, 0
  index: -1
mouth_2
  rotate: true
  xy: 1012, 662
  size: 32, 10
  orig: 32, 10
  offset: 0, 0
  index: -1
mouth_3
  rotate: true
  xy: 455, 293
  size: 37, 17
  orig: 37, 17
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 674, 278
  size: 88, 43
  orig: 88, 43
  offset: 0, 0
  index: -1
outerwear
  rotate: false
  xy: 922, 823
  size: 100, 118
  orig: 100, 118
  offset: 0, 0
  index: -1
trajectory_A
  rotate: true
  xy: 901, 259
  size: 64, 43
  orig: 64, 43
  offset: 0, 0
  index: -1
uparm_B
  rotate: false
  xy: 672, 131
  size: 31, 52
  orig: 31, 52
  offset: 0, 0
  index: -1
uparm_F
  rotate: true
  xy: 670, 185
  size: 33, 52
  orig: 33, 52
  offset: 0, 0
  index: -1
upleg_B
  rotate: false
  xy: 705, 130
  size: 29, 53
  orig: 29, 53
  offset: 0, 0
  index: -1
upleg_F
  rotate: false
  xy: 470, 61
  size: 28, 57
  orig: 28, 57
  offset: 0, 0
  index: -1
waist
  rotate: false
  xy: 547, 114
  size: 57, 49
  orig: 57, 49
  offset: 0, 0
  index: -1
waist_B
  rotate: false
  xy: 356, 120
  size: 113, 116
  orig: 113, 116
  offset: 0, 0
  index: -1
wave_ball1
  rotate: false
  xy: 839, 762
  size: 81, 127
  orig: 81, 127
  offset: 0, 0
  index: -1
wave_ball2
  rotate: false
  xy: 843, 633
  size: 81, 127
  orig: 81, 127
  offset: 0, 0
  index: -1
wave_ball3
  rotate: false
  xy: 846, 504
  size: 81, 127
  orig: 81, 127
  offset: 0, 0
  index: -1
wave_ball4
  rotate: false
  xy: 929, 567
  size: 81, 127
  orig: 81, 127
  offset: 0, 0
  index: -1
weapon_01
  rotate: false
  xy: 326, 957
  size: 370, 65
  orig: 370, 65
  offset: 0, 0
  index: -1
white
  rotate: false
  xy: 1012, 650
  size: 10, 10
  orig: 10, 10
  offset: 0, 0
  index: -1
