
sd_u_1109130.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Thunder_A1
  rotate: true
  xy: 658, 799
  size: 46, 179
  orig: 46, 179
  offset: 0, 0
  index: -1
Thunder_B1
  rotate: false
  xy: 180, 118
  size: 49, 169
  orig: 49, 169
  offset: 0, 0
  index: -1
Thunder_C1
  rotate: true
  xy: 454, 786
  size: 47, 185
  orig: 47, 185
  offset: 0, 0
  index: -1
Thunder_D1
  rotate: true
  xy: 628, 438
  size: 56, 133
  orig: 56, 133
  offset: 0, 0
  index: -1
body_B
  rotate: false
  xy: 833, 620
  size: 142, 139
  orig: 142, 139
  offset: 0, 0
  index: -1
body_F
  rotate: false
  xy: 454, 835
  size: 202, 187
  orig: 202, 187
  offset: 0, 0
  index: -1
bubble_01
  rotate: false
  xy: 218, 392
  size: 125, 92
  orig: 125, 92
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 771, 488
  size: 130, 130
  orig: 130, 130
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 634, 349
  size: 87, 87
  orig: 87, 87
  offset: 0, 0
  index: -1
cloak
  rotate: false
  xy: 2, 289
  size: 207, 188
  orig: 207, 188
  offset: 0, 0
  index: -1
collar
  rotate: false
  xy: 260, 792
  size: 192, 230
  orig: 192, 230
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 260, 646
  size: 178, 144
  orig: 178, 144
  offset: 0, 0
  index: -1
dead_f
  rotate: false
  xy: 440, 637
  size: 172, 147
  orig: 172, 147
  offset: 0, 0
  index: -1
ear_B
  rotate: false
  xy: 839, 319
  size: 81, 49
  orig: 81, 49
  offset: 0, 0
  index: -1
ef_circle_A
  rotate: false
  xy: 447, 183
  size: 102, 102
  orig: 102, 102
  offset: 0, 0
  index: -1
ef_circle_B
  rotate: false
  xy: 362, 295
  size: 104, 104
  orig: 104, 104
  offset: 0, 0
  index: -1
ef_cloud
  rotate: true
  xy: 550, 475
  size: 160, 76
  orig: 160, 76
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 470, 401
  size: 76, 76
  orig: 76, 76
  offset: 0, 0
  index: -1
ef_grd_001
  rotate: true
  xy: 701, 639
  size: 22, 39
  orig: 22, 39
  offset: 0, 0
  index: -1
ef_kira_A
  rotate: false
  xy: 548, 406
  size: 66, 67
  orig: 66, 67
  offset: 0, 0
  index: -1
ef_light_common_A
  rotate: false
  xy: 639, 175
  size: 35, 35
  orig: 35, 35
  offset: 0, 0
  index: -1
ef_light_common_B
  rotate: false
  xy: 218, 655
  size: 35, 36
  orig: 35, 36
  offset: 0, 0
  index: -1
ef_particle_WH020
  rotate: true
  xy: 124, 2
  size: 107, 106
  orig: 107, 106
  offset: 0, 0
  index: -1
ef_particle_WH021
  rotate: false
  xy: 639, 212
  size: 82, 80
  orig: 82, 80
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: true
  xy: 2, 2
  size: 107, 120
  orig: 107, 120
  offset: 0, 0
  index: -1
ef_particle_unatt_003a
  rotate: false
  xy: 218, 486
  size: 172, 158
  orig: 172, 158
  offset: 0, 0
  index: -1
ef_particle_unatt_003b
  rotate: false
  xy: 701, 663
  size: 130, 134
  orig: 130, 134
  offset: 0, 0
  index: -1
ef_particle_unatt_003c
  rotate: false
  xy: 723, 363
  size: 100, 57
  orig: 100, 57
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 628, 496
  size: 141, 141
  orig: 141, 141
  offset: 0, 0
  index: -1
ef_snow_field
  rotate: false
  xy: 658, 847
  size: 175, 175
  orig: 175, 175
  offset: 0, 0
  index: -1
ef_spotlight
  rotate: true
  xy: 825, 370
  size: 50, 93
  orig: 50, 93
  offset: 0, 0
  index: -1
ef_vortex_A
  rotate: false
  xy: 835, 848
  size: 174, 174
  orig: 174, 174
  offset: 0, 0
  index: -1
ef_vortex_B
  rotate: false
  xy: 2, 111
  size: 176, 176
  orig: 176, 176
  offset: 0, 0
  index: -1
ef_vortex_C
  rotate: false
  xy: 392, 479
  size: 156, 156
  orig: 156, 156
  offset: 0, 0
  index: -1
eye_1
  rotate: true
  xy: 326, 164
  size: 123, 61
  orig: 123, 61
  offset: 0, 0
  index: -1
eye_2
  rotate: true
  xy: 389, 173
  size: 120, 56
  orig: 120, 56
  offset: 0, 0
  index: -1
eye_3
  rotate: false
  xy: 763, 422
  size: 126, 64
  orig: 126, 64
  offset: 0, 0
  index: -1
foot_back_B_1
  rotate: false
  xy: 211, 289
  size: 97, 101
  orig: 97, 101
  offset: 0, 0
  index: -1
foot_back_B_2
  rotate: false
  xy: 310, 289
  size: 50, 101
  orig: 50, 101
  offset: 0, 0
  index: -1
foot_back_F_1
  rotate: false
  xy: 232, 58
  size: 90, 102
  orig: 90, 102
  offset: 0, 0
  index: -1
foot_back_F_2
  rotate: true
  xy: 745, 302
  size: 59, 92
  orig: 59, 92
  offset: 0, 0
  index: -1
foot_front_ B_1
  rotate: false
  xy: 468, 287
  size: 81, 112
  orig: 81, 112
  offset: 0, 0
  index: -1
foot_front_ B_2
  rotate: true
  xy: 634, 294
  size: 53, 109
  orig: 53, 109
  offset: 0, 0
  index: -1
foot_front_F_1
  rotate: false
  xy: 551, 293
  size: 81, 111
  orig: 81, 111
  offset: 0, 0
  index: -1
foot_front_F_2
  rotate: true
  xy: 232, 2
  size: 54, 113
  orig: 54, 113
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 693
  size: 256, 329
  orig: 256, 329
  offset: 0, 0
  index: -1
horn
  rotate: false
  xy: 614, 639
  size: 85, 145
  orig: 85, 145
  offset: 0, 0
  index: -1
mouth_under
  rotate: false
  xy: 345, 401
  size: 123, 76
  orig: 123, 76
  offset: 0, 0
  index: -1
mouth_upper
  rotate: true
  xy: 231, 162
  size: 125, 93
  orig: 125, 93
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 2, 479
  size: 214, 212
  orig: 214, 212
  offset: 0, 0
  index: -1
neck_acc
  rotate: true
  xy: 551, 185
  size: 106, 86
  orig: 106, 86
  offset: 0, 0
  index: -1
snow_slime
  rotate: true
  xy: 903, 490
  size: 128, 104
  orig: 128, 104
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 839, 761
  size: 159, 85
  orig: 159, 85
  offset: 0, 0
  index: -1
white
  rotate: false
  xy: 1011, 1011
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
