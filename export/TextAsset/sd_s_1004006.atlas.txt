
sd_s_1004006.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: true
  xy: 688, 437
  size: 55, 107
  orig: 55, 107
  offset: 0, 0
  index: -1
arm_F
  rotate: true
  xy: 688, 494
  size: 57, 131
  orig: 57, 131
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 2, 16
  size: 104, 142
  orig: 104, 142
  offset: 0, 0
  index: -1
brow_1
  rotate: true
  xy: 473, 91
  size: 140, 70
  orig: 140, 70
  offset: 0, 0
  index: -1
brow_2
  rotate: true
  xy: 536, 391
  size: 166, 39
  orig: 166, 39
  offset: 0, 0
  index: -1
brow_3
  rotate: true
  xy: 545, 98
  size: 140, 77
  orig: 140, 77
  offset: 0, 0
  index: -1
bubble_01
  rotate: false
  xy: 398, 559
  size: 178, 131
  orig: 178, 131
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 830, 569
  size: 185, 186
  orig: 185, 186
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 347, 107
  size: 124, 124
  orig: 124, 124
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 2, 122
  size: 343, 164
  orig: 343, 164
  offset: 0, 0
  index: -1
dead_f
  rotate: false
  xy: 626, 757
  size: 283, 155
  orig: 283, 155
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 577, 442
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: false
  xy: 381, 386
  size: 153, 171
  orig: 153, 171
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 626, 553
  size: 202, 202
  orig: 202, 202
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 381, 233
  size: 159, 151
  orig: 159, 151
  offset: 0, 0
  index: -1
eye_1
  rotate: false
  xy: 146, 2
  size: 146, 53
  orig: 146, 53
  offset: 0, 0
  index: -1
eye_2
  rotate: false
  xy: 830, 542
  size: 168, 25
  orig: 168, 25
  offset: 0, 0
  index: -1
eye_3
  rotate: false
  xy: 146, 57
  size: 182, 63
  orig: 182, 63
  offset: 0, 0
  index: -1
hair_B
  rotate: false
  xy: 2, 650
  size: 394, 372
  orig: 394, 372
  offset: 0, 0
  index: -1
hair_F
  rotate: false
  xy: 2, 288
  size: 377, 360
  orig: 377, 360
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 398, 692
  size: 226, 220
  orig: 226, 220
  offset: 0, 0
  index: -1
leg_B
  rotate: true
  xy: 577, 391
  size: 49, 100
  orig: 49, 100
  offset: 0, 0
  index: -1
leg_F
  rotate: true
  xy: 821, 488
  size: 52, 106
  orig: 52, 106
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 911, 758
  size: 57, 30
  orig: 57, 30
  offset: 0, 0
  index: -1
mouth_2
  rotate: false
  xy: 929, 523
  size: 64, 17
  orig: 64, 17
  offset: 0, 0
  index: -1
mouth_3
  rotate: true
  xy: 578, 630
  size: 60, 25
  orig: 60, 25
  offset: 0, 0
  index: -1
neck
  rotate: true
  xy: 911, 790
  size: 122, 98
  orig: 122, 98
  offset: 0, 0
  index: -1
uparm_B
  rotate: false
  xy: 542, 240
  size: 67, 149
  orig: 67, 149
  offset: 0, 0
  index: -1
uparm_F
  rotate: false
  xy: 545, 2
  size: 57, 94
  orig: 57, 94
  offset: 0, 0
  index: -1
upleg_B
  rotate: true
  xy: 679, 380
  size: 55, 101
  orig: 55, 101
  offset: 0, 0
  index: -1
upleg_F
  rotate: true
  xy: 330, 44
  size: 61, 98
  orig: 61, 98
  offset: 0, 0
  index: -1
waist
  rotate: false
  xy: 675, 294
  size: 99, 84
  orig: 99, 84
  offset: 0, 0
  index: -1
waist_F
  rotate: true
  xy: 611, 269
  size: 120, 62
  orig: 120, 62
  offset: 0, 0
  index: -1
weapon_01
  rotate: false
  xy: 398, 914
  size: 616, 108
  orig: 616, 108
  offset: 0, 0
  index: -1
