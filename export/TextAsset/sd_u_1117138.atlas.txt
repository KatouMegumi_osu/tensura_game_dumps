
sd_u_1117138.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: false
  xy: 667, 125
  size: 41, 83
  orig: 41, 83
  offset: 0, 0
  index: -1
arm_F
  rotate: false
  xy: 661, 38
  size: 47, 85
  orig: 47, 85
  offset: 0, 0
  index: -1
aura_A
  rotate: true
  xy: 539, 44
  size: 82, 67
  orig: 82, 67
  offset: 0, 0
  index: -1
aura_B
  rotate: false
  xy: 380, 225
  size: 85, 139
  orig: 85, 139
  offset: 0, 0
  index: -1
bandana_B
  rotate: false
  xy: 453, 49
  size: 84, 77
  orig: 84, 77
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 182, 17
  size: 88, 102
  orig: 88, 102
  offset: 0, 0
  index: -1
brow_1
  rotate: true
  xy: 749, 86
  size: 75, 17
  orig: 75, 17
  offset: 0, 0
  index: -1
brow_2
  rotate: false
  xy: 2, 2
  size: 71, 15
  orig: 71, 15
  offset: 0, 0
  index: -1
brow_3
  rotate: false
  xy: 272, 12
  size: 62, 18
  orig: 62, 18
  offset: 0, 0
  index: -1
bubble_01
  rotate: false
  xy: 349, 131
  size: 125, 92
  orig: 125, 92
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 467, 236
  size: 130, 130
  orig: 130, 130
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 364, 42
  size: 87, 87
  orig: 87, 87
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 2, 19
  size: 178, 100
  orig: 178, 100
  offset: 0, 0
  index: -1
dead_f
  rotate: false
  xy: 227, 269
  size: 151, 95
  orig: 151, 95
  offset: 0, 0
  index: -1
ed_slash
  rotate: false
  xy: 996, 353
  size: 18, 157
  orig: 18, 157
  offset: 0, 0
  index: -1
ef_aura3
  rotate: false
  xy: 706, 383
  size: 24, 40
  orig: 24, 40
  offset: 0, 0
  index: -1
ef_gld_line
  rotate: false
  xy: 943, 352
  size: 51, 158
  orig: 51, 158
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 589, 132
  size: 76, 76
  orig: 76, 76
  offset: 0, 0
  index: -1
ef_grain_unatt_003
  rotate: false
  xy: 828, 233
  size: 113, 113
  orig: 113, 113
  offset: 0, 0
  index: -1
ef_kira_A
  rotate: false
  xy: 710, 163
  size: 66, 67
  orig: 66, 67
  offset: 0, 0
  index: -1
ef_light_common_A
  rotate: false
  xy: 426, 5
  size: 35, 35
  orig: 35, 35
  offset: 0, 0
  index: -1
ef_light_common_B
  rotate: false
  xy: 784, 130
  size: 35, 36
  orig: 35, 36
  offset: 0, 0
  index: -1
ef_particle_WH021
  rotate: false
  xy: 708, 232
  size: 118, 114
  orig: 118, 114
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: false
  xy: 599, 246
  size: 107, 120
  orig: 107, 120
  offset: 0, 0
  index: -1
ef_particle_unatt_002b
  rotate: false
  xy: 2, 302
  size: 223, 208
  orig: 223, 208
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 206, 126
  size: 141, 141
  orig: 141, 141
  offset: 0, 0
  index: -1
ef_ring
  rotate: true
  xy: 608, 35
  size: 95, 51
  orig: 95, 51
  offset: 0, 0
  index: -1
ef_slash_unatt_001
  rotate: false
  xy: 227, 366
  size: 234, 144
  orig: 234, 144
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 476, 128
  size: 111, 106
  orig: 111, 106
  offset: 0, 0
  index: -1
eye_1
  rotate: false
  xy: 599, 210
  size: 104, 34
  orig: 104, 34
  offset: 0, 0
  index: -1
eye_2
  rotate: true
  xy: 990, 232
  size: 118, 30
  orig: 118, 30
  offset: 0, 0
  index: -1
eye_3
  rotate: true
  xy: 943, 229
  size: 121, 45
  orig: 121, 45
  offset: 0, 0
  index: -1
hair_F
  rotate: false
  xy: 2, 121
  size: 202, 179
  orig: 202, 179
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 741, 348
  size: 200, 162
  orig: 200, 162
  offset: 0, 0
  index: -1
leg_B
  rotate: true
  xy: 661, 2
  size: 34, 75
  orig: 34, 75
  offset: 0, 0
  index: -1
leg_F
  rotate: false
  xy: 710, 81
  size: 37, 80
  orig: 37, 80
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 738, 2
  size: 48, 36
  orig: 48, 36
  offset: 0, 0
  index: -1
mouth_2
  rotate: true
  xy: 768, 104
  size: 57, 14
  orig: 57, 14
  offset: 0, 0
  index: -1
mouth_3
  rotate: false
  xy: 75, 2
  size: 53, 15
  orig: 53, 15
  offset: 0, 0
  index: -1
move_A
  rotate: false
  xy: 463, 425
  size: 276, 85
  orig: 276, 85
  offset: 0, 0
  index: -1
uparm_B
  rotate: true
  xy: 835, 144
  size: 43, 64
  orig: 43, 64
  offset: 0, 0
  index: -1
uparm_F
  rotate: true
  xy: 710, 40
  size: 39, 57
  orig: 39, 57
  offset: 0, 0
  index: -1
upleg_B
  rotate: true
  xy: 364, 3
  size: 37, 60
  orig: 37, 60
  offset: 0, 0
  index: -1
upleg_F
  rotate: true
  xy: 835, 189
  size: 42, 73
  orig: 42, 73
  offset: 0, 0
  index: -1
waist
  rotate: true
  xy: 778, 168
  size: 62, 55
  orig: 62, 55
  offset: 0, 0
  index: -1
waist_F
  rotate: true
  xy: 272, 32
  size: 92, 90
  orig: 92, 90
  offset: 0, 0
  index: -1
weapon_01
  rotate: false
  xy: 463, 368
  size: 241, 55
  orig: 241, 55
  offset: 0, 0
  index: -1
white
  rotate: false
  xy: 206, 289
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
