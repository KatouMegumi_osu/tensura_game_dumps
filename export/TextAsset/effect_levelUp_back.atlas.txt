
effect_levelUp_back.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
ball_m_01
  rotate: false
  xy: 2, 231
  size: 279, 279
  orig: 279, 279
  offset: 0, 0
  index: -1
ball_s_01
  rotate: false
  xy: 2, 2
  size: 53, 53
  orig: 53, 53
  offset: 0, 0
  index: -1
effect_comic_line
  rotate: false
  xy: 283, 240
  size: 270, 270
  orig: 270, 270
  offset: 0, 0
  index: -1
perticle_ball_01
  rotate: false
  xy: 2, 57
  size: 240, 172
  orig: 240, 172
  offset: 0, 0
  index: -1
