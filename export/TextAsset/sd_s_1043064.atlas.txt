
sd_s_1043064.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
apron
  rotate: true
  xy: 531, 611
  size: 94, 92
  orig: 94, 92
  offset: 0, 0
  index: -1
arm_B
  rotate: true
  xy: 835, 776
  size: 51, 110
  orig: 51, 110
  offset: 0, 0
  index: -1
arm_F
  rotate: false
  xy: 718, 610
  size: 55, 110
  orig: 55, 110
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 513, 707
  size: 110, 128
  orig: 110, 128
  offset: 0, 0
  index: -1
brow_1
  rotate: true
  xy: 188, 268
  size: 157, 56
  orig: 157, 56
  offset: 0, 0
  index: -1
brow_2
  rotate: true
  xy: 156, 77
  size: 186, 43
  orig: 186, 43
  offset: 0, 0
  index: -1
brow_3
  rotate: true
  xy: 201, 109
  size: 157, 56
  orig: 157, 56
  offset: 0, 0
  index: -1
bubble_01
  rotate: true
  xy: 855, 829
  size: 178, 131
  orig: 178, 131
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 298, 821
  size: 185, 186
  orig: 185, 186
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 298, 542
  size: 124, 124
  orig: 124, 124
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 298, 668
  size: 159, 151
  orig: 159, 151
  offset: 0, 0
  index: -1
eye_1
  rotate: true
  xy: 459, 666
  size: 153, 52
  orig: 153, 52
  offset: 0, 0
  index: -1
eye_2
  rotate: true
  xy: 988, 840
  size: 167, 28
  orig: 167, 28
  offset: 0, 0
  index: -1
eye_3
  rotate: false
  xy: 660, 791
  size: 173, 69
  orig: 173, 69
  offset: 0, 0
  index: -1
hat
  rotate: false
  xy: 2, 666
  size: 294, 341
  orig: 294, 341
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 427
  size: 215, 237
  orig: 215, 237
  offset: 0, 0
  index: -1
head_2
  rotate: false
  xy: 424, 548
  size: 105, 116
  orig: 105, 116
  offset: 0, 0
  index: -1
leg_B
  rotate: true
  xy: 625, 722
  size: 67, 156
  orig: 67, 156
  offset: 0, 0
  index: -1
leg_F
  rotate: false
  xy: 219, 490
  size: 77, 174
  orig: 77, 174
  offset: 0, 0
  index: -1
mouth_1
  rotate: true
  xy: 660, 862
  size: 145, 193
  orig: 145, 193
  offset: 0, 0
  index: -1
mouth_2
  rotate: false
  xy: 485, 837
  size: 173, 170
  orig: 173, 170
  offset: 0, 0
  index: -1
mouth_3
  rotate: false
  xy: 2, 75
  size: 152, 188
  orig: 152, 188
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 2, 265
  size: 184, 160
  orig: 184, 160
  offset: 0, 0
  index: -1
uparm_B
  rotate: false
  xy: 628, 535
  size: 55, 93
  orig: 55, 93
  offset: 0, 0
  index: -1
uparm_F
  rotate: true
  xy: 531, 542
  size: 67, 95
  orig: 67, 95
  offset: 0, 0
  index: -1
upleg_B
  rotate: true
  xy: 2, 2
  size: 71, 157
  orig: 71, 157
  offset: 0, 0
  index: -1
upleg_F
  rotate: true
  xy: 298, 471
  size: 69, 123
  orig: 69, 123
  offset: 0, 0
  index: -1
waist
  rotate: false
  xy: 625, 630
  size: 91, 90
  orig: 91, 90
  offset: 0, 0
  index: -1
