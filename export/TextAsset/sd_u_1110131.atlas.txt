
sd_u_1110131.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
T_body
  rotate: false
  xy: 553, 487
  size: 73, 39
  orig: 73, 39
  offset: 0, 0
  index: -1
T_body_blur
  rotate: false
  xy: 134, 2
  size: 84, 50
  orig: 84, 50
  offset: 0, 0
  index: -1
T_head
  rotate: false
  xy: 729, 726
  size: 72, 52
  orig: 72, 52
  offset: 0, 0
  index: -1
T_head_blur
  rotate: false
  xy: 639, 670
  size: 83, 63
  orig: 83, 63
  offset: 0, 0
  index: -1
T_leg_B1
  rotate: true
  xy: 286, 296
  size: 19, 39
  orig: 19, 39
  offset: 0, 0
  index: -1
T_leg_B1_blur
  rotate: false
  xy: 822, 906
  size: 29, 49
  orig: 29, 49
  offset: 0, 0
  index: -1
T_leg_B2
  rotate: false
  xy: 89, 9
  size: 32, 35
  orig: 32, 35
  offset: 0, 0
  index: -1
T_leg_B2_blur
  rotate: false
  xy: 963, 976
  size: 43, 46
  orig: 43, 46
  offset: 0, 0
  index: -1
T_leg_F1
  rotate: true
  xy: 853, 883
  size: 25, 34
  orig: 25, 34
  offset: 0, 0
  index: -1
T_leg_F1_blur
  rotate: false
  xy: 906, 926
  size: 35, 45
  orig: 35, 45
  offset: 0, 0
  index: -1
T_leg_F2
  rotate: false
  xy: 967, 950
  size: 36, 24
  orig: 36, 24
  offset: 0, 0
  index: -1
T_leg_F2_blur
  rotate: false
  xy: 805, 870
  size: 46, 34
  orig: 46, 34
  offset: 0, 0
  index: -1
T_neck
  rotate: false
  xy: 614, 645
  size: 23, 45
  orig: 23, 45
  offset: 0, 0
  index: -1
T_neck_blur
  rotate: false
  xy: 853, 910
  size: 34, 55
  orig: 34, 55
  offset: 0, 0
  index: -1
T_tail
  rotate: false
  xy: 134, 157
  size: 12, 19
  orig: 12, 19
  offset: 0, 0
  index: -1
T_tail_blur
  rotate: false
  xy: 702, 638
  size: 23, 30
  orig: 23, 30
  offset: 0, 0
  index: -1
arm_B
  rotate: true
  xy: 2, 2
  size: 42, 85
  orig: 42, 85
  offset: 0, 0
  index: -1
arm_F
  rotate: true
  xy: 220, 2
  size: 50, 84
  orig: 50, 84
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 239, 205
  size: 74, 88
  orig: 74, 88
  offset: 0, 0
  index: -1
brow_1
  rotate: true
  xy: 2, 305
  size: 62, 13
  orig: 62, 13
  offset: 0, 0
  index: -1
brow_2
  rotate: true
  xy: 889, 909
  size: 56, 15
  orig: 56, 15
  offset: 0, 0
  index: -1
brow_3
  rotate: true
  xy: 436, 824
  size: 54, 16
  orig: 54, 16
  offset: 0, 0
  index: -1
bubble_01
  rotate: true
  xy: 331, 600
  size: 125, 92
  orig: 125, 92
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 2, 46
  size: 130, 130
  orig: 130, 130
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 553, 735
  size: 87, 87
  orig: 87, 87
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 193, 727
  size: 182, 94
  orig: 182, 94
  offset: 0, 0
  index: -1
dead_f
  rotate: false
  xy: 2, 178
  size: 148, 115
  orig: 148, 115
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 536, 614
  size: 76, 76
  orig: 76, 76
  offset: 0, 0
  index: -1
ef_grain_unatt_001
  rotate: false
  xy: 182, 295
  size: 102, 101
  orig: 102, 101
  offset: 0, 0
  index: -1
ef_grain_unatt_003
  rotate: false
  xy: 425, 600
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
ef_grain_unatt_004
  rotate: false
  xy: 650, 861
  size: 153, 153
  orig: 153, 153
  offset: 0, 0
  index: -1
ef_grain_unatt_007
  rotate: false
  xy: 188, 584
  size: 141, 141
  orig: 141, 141
  offset: 0, 0
  index: -1
ef_grd_001
  rotate: false
  xy: 943, 932
  size: 22, 39
  orig: 22, 39
  offset: 0, 0
  index: -1
ef_kira_B
  rotate: false
  xy: 152, 208
  size: 85, 85
  orig: 85, 85
  offset: 0, 0
  index: -1
ef_linework_A2
  rotate: false
  xy: 2, 762
  size: 189, 260
  orig: 189, 260
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: false
  xy: 336, 478
  size: 107, 120
  orig: 107, 120
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 193, 441
  size: 141, 141
  orig: 141, 141
  offset: 0, 0
  index: -1
ef_ring
  rotate: false
  xy: 458, 824
  size: 190, 190
  orig: 190, 190
  offset: 0, 0
  index: -1
ef_star
  rotate: false
  xy: 440, 711
  size: 111, 111
  orig: 111, 111
  offset: 0, 0
  index: -1
effect_bow
  rotate: true
  xy: 445, 487
  size: 111, 106
  orig: 111, 106
  offset: 0, 0
  index: -1
eye_1
  rotate: false
  xy: 308, 406
  size: 95, 33
  orig: 95, 33
  offset: 0, 0
  index: -1
eye_2
  rotate: true
  xy: 805, 910
  size: 104, 15
  orig: 104, 15
  offset: 0, 0
  index: -1
eye_3
  rotate: false
  xy: 336, 441
  size: 104, 35
  orig: 104, 35
  offset: 0, 0
  index: -1
fire_powder_A
  rotate: false
  xy: 239, 125
  size: 74, 78
  orig: 74, 78
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 21, 523
  size: 237, 165
  orig: 237, 165
  offset: 0, 0
  index: -1
head_acc
  rotate: false
  xy: 650, 780
  size: 144, 79
  orig: 144, 79
  offset: 0, 0
  index: -1
hut
  rotate: false
  xy: 193, 880
  size: 263, 142
  orig: 263, 142
  offset: 0, 0
  index: -1
leg_B
  rotate: true
  xy: 193, 398
  size: 41, 113
  orig: 41, 113
  offset: 0, 0
  index: -1
leg_F
  rotate: false
  xy: 286, 317
  size: 44, 79
  orig: 44, 79
  offset: 0, 0
  index: -1
mouth_1
  rotate: true
  xy: 822, 957
  size: 57, 27
  orig: 57, 27
  offset: 0, 0
  index: -1
mouth_2
  rotate: true
  xy: 299, 66
  size: 57, 22
  orig: 57, 22
  offset: 0, 0
  index: -1
mouth_3
  rotate: false
  xy: 639, 639
  size: 61, 29
  orig: 61, 29
  offset: 0, 0
  index: -1
nose
  rotate: false
  xy: 906, 973
  size: 55, 49
  orig: 55, 49
  offset: 0, 0
  index: -1
present_back
  rotate: false
  xy: 642, 735
  size: 85, 43
  orig: 85, 43
  offset: 0, 0
  index: -1
present_cover1
  rotate: true
  xy: 377, 728
  size: 93, 61
  orig: 93, 61
  offset: 0, 0
  index: -1
present_cover2
  rotate: false
  xy: 152, 123
  size: 85, 83
  orig: 85, 83
  offset: 0, 0
  index: -1
present_flont
  rotate: false
  xy: 134, 54
  size: 85, 67
  orig: 85, 67
  offset: 0, 0
  index: -1
rope
  rotate: true
  xy: 458, 1016
  size: 6, 379
  orig: 6, 379
  offset: 0, 0
  index: -1
rope_bler
  rotate: false
  xy: 2, 369
  size: 17, 391
  orig: 17, 391
  offset: 0, 0
  index: -1
santa
  rotate: false
  xy: 21, 295
  size: 159, 106
  orig: 159, 106
  offset: 0, 0
  index: -1
santa_blur
  rotate: false
  xy: 21, 403
  size: 170, 118
  orig: 170, 118
  offset: 0, 0
  index: -1
santa_hat
  rotate: true
  xy: 425, 340
  size: 64, 36
  orig: 64, 36
  offset: 0, 0
  index: -1
santa_hat_blur
  rotate: false
  xy: 724, 677
  size: 76, 47
  orig: 76, 47
  offset: 0, 0
  index: -1
uparm_B
  rotate: true
  xy: 553, 692
  size: 41, 84
  orig: 41, 84
  offset: 0, 0
  index: -1
uparm_F
  rotate: true
  xy: 405, 406
  size: 33, 57
  orig: 33, 57
  offset: 0, 0
  index: -1
upleg_B
  rotate: false
  xy: 332, 330
  size: 41, 74
  orig: 41, 74
  offset: 0, 0
  index: -1
upleg_F
  rotate: false
  xy: 375, 337
  size: 48, 67
  orig: 48, 67
  offset: 0, 0
  index: -1
waist
  rotate: false
  xy: 221, 54
  size: 76, 67
  orig: 76, 67
  offset: 0, 0
  index: -1
waist_F
  rotate: true
  xy: 553, 528
  size: 84, 60
  orig: 84, 60
  offset: 0, 0
  index: -1
waist_acc
  rotate: false
  xy: 851, 967
  size: 53, 55
  orig: 53, 55
  offset: 0, 0
  index: -1
weapon_01
  rotate: false
  xy: 193, 823
  size: 241, 55
  orig: 241, 55
  offset: 0, 0
  index: -1
white
  rotate: false
  xy: 536, 698
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
