
sd_u_1108129.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: false
  xy: 943, 449
  size: 40, 84
  orig: 40, 84
  offset: 0, 0
  index: -1
arm_F
  rotate: false
  xy: 983, 822
  size: 39, 84
  orig: 39, 84
  offset: 0, 0
  index: -1
bangs_B
  rotate: false
  xy: 741, 834
  size: 217, 188
  orig: 217, 188
  offset: 0, 0
  index: -1
bangs_F
  rotate: true
  xy: 136, 436
  size: 97, 207
  orig: 97, 207
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 4
  size: 72, 107
  orig: 72, 107
  offset: 0, 0
  index: -1
brow_1
  rotate: false
  xy: 59, 121
  size: 32, 16
  orig: 32, 16
  offset: 0, 0
  index: -1
brow_2
  rotate: false
  xy: 559, 129
  size: 36, 15
  orig: 36, 15
  offset: 0, 0
  index: -1
brow_3
  rotate: false
  xy: 983, 732
  size: 30, 20
  orig: 30, 20
  offset: 0, 0
  index: -1
bubble_01
  rotate: true
  xy: 329, 168
  size: 125, 92
  orig: 125, 92
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 577, 272
  size: 130, 130
  orig: 130, 130
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 330, 79
  size: 87, 87
  orig: 87, 87
  offset: 0, 0
  index: -1
collar_F
  rotate: false
  xy: 499, 2
  size: 25, 78
  orig: 25, 78
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 718, 729
  size: 204, 103
  orig: 204, 103
  offset: 0, 0
  index: -1
dead_f
  rotate: true
  xy: 836, 534
  size: 193, 104
  orig: 193, 104
  offset: 0, 0
  index: -1
ef_aura_souei_a001
  rotate: false
  xy: 942, 535
  size: 41, 111
  orig: 41, 111
  offset: 0, 0
  index: -1
ef_aura_souei_a002
  rotate: false
  xy: 960, 908
  size: 46, 114
  orig: 46, 114
  offset: 0, 0
  index: -1
ef_aura_souei_a003
  rotate: false
  xy: 559, 177
  size: 34, 82
  orig: 34, 82
  offset: 0, 0
  index: -1
ef_blizzard_A
  rotate: false
  xy: 198, 871
  size: 279, 151
  orig: 279, 151
  offset: 0, 0
  index: -1
ef_blood_blur_A
  rotate: true
  xy: 302, 295
  size: 113, 130
  orig: 113, 130
  offset: 0, 0
  index: -1
ef_blood_blur_B
  rotate: false
  xy: 635, 428
  size: 104, 124
  orig: 104, 124
  offset: 0, 0
  index: -1
ef_blood_blur_C
  rotate: false
  xy: 835, 450
  size: 106, 82
  orig: 106, 82
  offset: 0, 0
  index: -1
ef_blood_blur_D
  rotate: false
  xy: 419, 82
  size: 111, 45
  orig: 111, 45
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 421, 4
  size: 76, 76
  orig: 76, 76
  offset: 0, 0
  index: -1
ef_grd_001
  rotate: true
  xy: 635, 404
  size: 22, 39
  orig: 22, 39
  offset: 0, 0
  index: -1
ef_kira
  rotate: false
  xy: 1008, 931
  size: 14, 91
  orig: 14, 91
  offset: 0, 0
  index: -1
ef_light_common_A
  rotate: false
  xy: 526, 5
  size: 35, 35
  orig: 35, 35
  offset: 0, 0
  index: -1
ef_particle_WH020
  rotate: true
  xy: 222, 52
  size: 107, 106
  orig: 107, 106
  offset: 0, 0
  index: -1
ef_particle_WH021
  rotate: true
  xy: 942, 648
  size: 82, 80
  orig: 82, 80
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: true
  xy: 100, 52
  size: 107, 120
  orig: 107, 120
  offset: 0, 0
  index: -1
ef_particle_unatt_003a
  rotate: false
  xy: 662, 569
  size: 172, 158
  orig: 172, 158
  offset: 0, 0
  index: -1
ef_particle_unatt_003b
  rotate: true
  xy: 423, 129
  size: 130, 134
  orig: 130, 134
  offset: 0, 0
  index: -1
ef_particle_unatt_003c
  rotate: true
  xy: 924, 732
  size: 100, 57
  orig: 100, 57
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 434, 261
  size: 141, 141
  orig: 141, 141
  offset: 0, 0
  index: -1
ef_slash_claw
  rotate: false
  xy: 100, 281
  size: 200, 153
  orig: 200, 153
  offset: 0, 0
  index: -1
ef_slash_unatt_001
  rotate: false
  xy: 479, 861
  size: 260, 161
  orig: 260, 161
  offset: 0, 0
  index: -1
ef_slash_unatt_002
  rotate: false
  xy: 198, 751
  size: 258, 118
  orig: 258, 118
  offset: 0, 0
  index: -1
ef_slash_unatt_003
  rotate: false
  xy: 458, 741
  size: 258, 118
  orig: 258, 118
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 345, 410
  size: 111, 106
  orig: 111, 106
  offset: 0, 0
  index: -1
eye_1
  rotate: false
  xy: 526, 42
  size: 34, 38
  orig: 34, 38
  offset: 0, 0
  index: -1
eye_2
  rotate: true
  xy: 960, 860
  size: 46, 21
  orig: 46, 21
  offset: 0, 0
  index: -1
eye_3
  rotate: false
  xy: 532, 82
  size: 45, 45
  orig: 45, 45
  offset: 0, 0
  index: -1
hair
  rotate: false
  xy: 136, 535
  size: 207, 214
  orig: 207, 214
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 458, 404
  size: 175, 148
  orig: 175, 148
  offset: 0, 0
  index: -1
head_acc
  rotate: true
  xy: 330, 31
  size: 46, 89
  orig: 46, 89
  offset: 0, 0
  index: -1
leg_B
  rotate: true
  xy: 559, 146
  size: 29, 75
  orig: 29, 75
  offset: 0, 0
  index: -1
leg_F
  rotate: true
  xy: 595, 177
  size: 32, 76
  orig: 32, 76
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 676, 410
  size: 32, 16
  orig: 32, 16
  offset: 0, 0
  index: -1
mouth_2
  rotate: false
  xy: 710, 419
  size: 29, 7
  orig: 29, 7
  offset: 0, 0
  index: -1
mouth_3
  rotate: false
  xy: 662, 555
  size: 27, 12
  orig: 27, 12
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 664, 217
  size: 36, 53
  orig: 36, 53
  offset: 0, 0
  index: -1
outer_B
  rotate: true
  xy: 835, 412
  size: 36, 93
  orig: 36, 93
  offset: 0, 0
  index: -1
outer_B2
  rotate: false
  xy: 741, 453
  size: 92, 114
  orig: 92, 114
  offset: 0, 0
  index: -1
outer_F
  rotate: true
  xy: 741, 417
  size: 34, 92
  orig: 34, 92
  offset: 0, 0
  index: -1
outer_acc
  rotate: true
  xy: 302, 410
  size: 24, 32
  orig: 24, 32
  offset: 0, 0
  index: -1
uparm_B
  rotate: false
  xy: 985, 511
  size: 37, 62
  orig: 37, 62
  offset: 0, 0
  index: -1
uparm_F
  rotate: false
  xy: 983, 754
  size: 39, 66
  orig: 39, 66
  offset: 0, 0
  index: -1
upleg_B
  rotate: false
  xy: 985, 442
  size: 36, 67
  orig: 36, 67
  offset: 0, 0
  index: -1
upleg_F
  rotate: false
  xy: 985, 575
  size: 37, 71
  orig: 37, 71
  offset: 0, 0
  index: -1
waist
  rotate: false
  xy: 595, 211
  size: 67, 59
  orig: 67, 59
  offset: 0, 0
  index: -1
weapon_01
  rotate: true
  xy: 2, 113
  size: 331, 55
  orig: 331, 55
  offset: 0, 0
  index: -1
weapon_02
  rotate: true
  xy: 59, 139
  size: 305, 39
  orig: 305, 39
  offset: 0, 0
  index: -1
white
  rotate: false
  xy: 930, 437
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
zansou01
  rotate: false
  xy: 2, 757
  size: 194, 265
  orig: 194, 265
  offset: 0, 0
  index: -1
zansou02
  rotate: false
  xy: 100, 161
  size: 227, 118
  orig: 227, 118
  offset: 0, 0
  index: -1
zansou03
  rotate: false
  xy: 2, 446
  size: 132, 309
  orig: 132, 309
  offset: 0, 0
  index: -1
zansou04
  rotate: true
  xy: 345, 518
  size: 231, 111
  orig: 231, 111
  offset: 0, 0
  index: -1
zansou05
  rotate: false
  xy: 458, 554
  size: 202, 185
  orig: 202, 185
  offset: 0, 0
  index: -1
