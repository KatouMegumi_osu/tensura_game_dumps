
sd_s_1007010.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: true
  xy: 437, 206
  size: 61, 99
  orig: 61, 99
  offset: 0, 0
  index: -1
arm_F1
  rotate: false
  xy: 552, 740
  size: 42, 83
  orig: 42, 83
  offset: 0, 0
  index: -1
arm_F2
  rotate: true
  xy: 437, 269
  size: 63, 99
  orig: 63, 99
  offset: 0, 0
  index: -1
backhair
  rotate: true
  xy: 315, 343
  size: 67, 120
  orig: 67, 120
  offset: 0, 0
  index: -1
bangs
  rotate: false
  xy: 148, 518
  size: 121, 167
  orig: 121, 167
  offset: 0, 0
  index: -1
bangs_1
  rotate: false
  xy: 2, 738
  size: 164, 284
  orig: 164, 284
  offset: 0, 0
  index: -1
bangs_2
  rotate: true
  xy: 168, 687
  size: 117, 190
  orig: 117, 190
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 351, 181
  size: 71, 78
  orig: 71, 78
  offset: 0, 0
  index: -1
brow_1
  rotate: true
  xy: 545, 621
  size: 84, 14
  orig: 84, 14
  offset: 0, 0
  index: -1
brow_2
  rotate: false
  xy: 596, 737
  size: 76, 15
  orig: 76, 15
  offset: 0, 0
  index: -1
brow_3
  rotate: false
  xy: 735, 738
  size: 68, 22
  orig: 68, 22
  offset: 0, 0
  index: -1
bubble_01
  rotate: true
  xy: 926, 799
  size: 125, 92
  orig: 125, 92
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 315, 412
  size: 130, 130
  orig: 130, 130
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 348, 254
  size: 87, 87
  orig: 87, 87
  offset: 0, 0
  index: -1
dead_b
  rotate: true
  xy: 98, 6
  size: 144, 107
  orig: 144, 107
  offset: 0, 0
  index: -1
dead_f
  rotate: true
  xy: 2, 2
  size: 148, 94
  orig: 148, 94
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 596, 754
  size: 76, 76
  orig: 76, 76
  offset: 0, 0
  index: -1
ef_grain_fire_002
  rotate: false
  xy: 926, 926
  size: 96, 96
  orig: 96, 96
  offset: 0, 0
  index: -1
ef_grain_unatt_001
  rotate: false
  xy: 244, 222
  size: 102, 102
  orig: 102, 102
  offset: 0, 0
  index: -1
ef_grain_unatt_003
  rotate: false
  xy: 360, 825
  size: 197, 197
  orig: 197, 197
  offset: 0, 0
  index: -1
ef_grain_unatt_004
  rotate: false
  xy: 235, 326
  size: 55, 55
  orig: 55, 55
  offset: 0, 0
  index: -1
ef_grd_001
  rotate: true
  xy: 271, 520
  size: 22, 39
  orig: 22, 39
  offset: 0, 0
  index: -1
ef_kira_001
  rotate: false
  xy: 235, 383
  size: 78, 133
  orig: 78, 133
  offset: 0, 0
  index: -1
ef_kira_002
  rotate: true
  xy: 360, 707
  size: 116, 190
  orig: 116, 190
  offset: 0, 0
  index: -1
ef_kira_003
  rotate: false
  xy: 18, 312
  size: 128, 188
  orig: 128, 188
  offset: 0, 0
  index: -1
ef_lightline_3
  rotate: true
  xy: 2, 170
  size: 330, 14
  orig: 330, 14
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: false
  xy: 447, 442
  size: 107, 120
  orig: 107, 120
  offset: 0, 0
  index: -1
ef_particle_unatt_002b
  rotate: false
  xy: 18, 152
  size: 169, 158
  orig: 169, 158
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 271, 544
  size: 141, 141
  orig: 141, 141
  offset: 0, 0
  index: -1
ef_ring
  rotate: false
  xy: 559, 832
  size: 190, 190
  orig: 190, 190
  offset: 0, 0
  index: -1
ef_slash_unatt_001
  rotate: true
  xy: 2, 502
  size: 234, 144
  orig: 234, 144
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 447, 334
  size: 111, 106
  orig: 111, 106
  offset: 0, 0
  index: -1
eye_1
  rotate: false
  xy: 244, 184
  size: 105, 36
  orig: 105, 36
  offset: 0, 0
  index: -1
eye_2
  rotate: false
  xy: 751, 822
  size: 123, 22
  orig: 123, 22
  offset: 0, 0
  index: -1
eye_3
  rotate: true
  xy: 189, 195
  size: 129, 53
  orig: 129, 53
  offset: 0, 0
  index: -1
hair
  rotate: false
  xy: 168, 806
  size: 190, 216
  orig: 190, 216
  offset: 0, 0
  index: -1
hair_acc
  rotate: false
  xy: 538, 183
  size: 53, 52
  orig: 53, 52
  offset: 0, 0
  index: -1
hakama
  rotate: false
  xy: 414, 564
  size: 129, 141
  orig: 129, 141
  offset: 0, 0
  index: -1
hand_F
  rotate: false
  xy: 189, 158
  size: 35, 35
  orig: 35, 35
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 751, 846
  size: 173, 176
  orig: 173, 176
  offset: 0, 0
  index: -1
horn_B
  rotate: true
  xy: 431, 152
  size: 52, 90
  orig: 52, 90
  offset: 0, 0
  index: -1
horn_F
  rotate: false
  xy: 538, 237
  size: 47, 95
  orig: 47, 95
  offset: 0, 0
  index: -1
leg_B
  rotate: true
  xy: 226, 154
  size: 28, 72
  orig: 28, 72
  offset: 0, 0
  index: -1
leg_F
  rotate: false
  xy: 246, 82
  size: 27, 70
  orig: 27, 70
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 207, 2
  size: 20, 20
  orig: 20, 20
  offset: 0, 0
  index: -1
mouth_2
  rotate: false
  xy: 360, 696
  size: 27, 9
  orig: 27, 9
  offset: 0, 0
  index: -1
mouth_3
  rotate: true
  xy: 148, 707
  size: 29, 16
  orig: 29, 16
  offset: 0, 0
  index: -1
neck
  rotate: true
  xy: 674, 754
  size: 76, 59
  orig: 76, 59
  offset: 0, 0
  index: -1
sidehair
  rotate: false
  xy: 148, 326
  size: 85, 190
  orig: 85, 190
  offset: 0, 0
  index: -1
solid
  rotate: false
  xy: 2, 157
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
uparm_B
  rotate: true
  xy: 523, 141
  size: 40, 73
  orig: 40, 73
  offset: 0, 0
  index: -1
uparm_F2
  rotate: true
  xy: 207, 24
  size: 39, 64
  orig: 39, 64
  offset: 0, 0
  index: -1
upleg_B
  rotate: true
  xy: 805, 730
  size: 39, 69
  orig: 39, 69
  offset: 0, 0
  index: -1
upleg_F
  rotate: false
  xy: 207, 65
  size: 37, 74
  orig: 37, 74
  offset: 0, 0
  index: -1
waist
  rotate: false
  xy: 735, 762
  size: 68, 58
  orig: 68, 58
  offset: 0, 0
  index: -1
waist_acc
  rotate: false
  xy: 805, 771
  size: 65, 49
  orig: 65, 49
  offset: 0, 0
  index: -1
