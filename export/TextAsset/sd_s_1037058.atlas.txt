
sd_s_1037058.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: true
  xy: 321, 462
  size: 60, 119
  orig: 60, 119
  offset: 0, 0
  index: -1
arm_F
  rotate: true
  xy: 2, 27
  size: 63, 124
  orig: 63, 124
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 911, 847
  size: 107, 175
  orig: 107, 175
  offset: 0, 0
  index: -1
brow_1
  rotate: false
  xy: 432, 526
  size: 91, 34
  orig: 91, 34
  offset: 0, 0
  index: -1
brow_2
  rotate: false
  xy: 725, 779
  size: 95, 27
  orig: 95, 27
  offset: 0, 0
  index: -1
brow_3
  rotate: false
  xy: 348, 188
  size: 83, 46
  orig: 83, 46
  offset: 0, 0
  index: -1
bubble_01
  rotate: false
  xy: 558, 891
  size: 178, 131
  orig: 178, 131
  offset: 0, 0
  index: -1
bubble_02
  rotate: true
  xy: 144, 21
  size: 185, 186
  orig: 185, 186
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 457, 612
  size: 124, 124
  orig: 124, 124
  offset: 0, 0
  index: -1
dead_b
  rotate: true
  xy: 2, 92
  size: 318, 140
  orig: 318, 140
  offset: 0, 0
  index: -1
dead_f
  rotate: true
  xy: 321, 635
  size: 280, 134
  orig: 280, 134
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 321, 524
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: true
  xy: 738, 869
  size: 153, 171
  orig: 153, 171
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 144, 208
  size: 202, 202
  orig: 202, 202
  offset: 0, 0
  index: -1
effect_bow
  rotate: false
  xy: 517, 738
  size: 159, 151
  orig: 159, 151
  offset: 0, 0
  index: -1
eye_1
  rotate: false
  xy: 432, 562
  size: 156, 48
  orig: 156, 48
  offset: 0, 0
  index: -1
eye_2
  rotate: true
  xy: 457, 738
  size: 177, 58
  orig: 177, 58
  offset: 0, 0
  index: -1
eye_3
  rotate: true
  xy: 270, 430
  size: 175, 49
  orig: 175, 49
  offset: 0, 0
  index: -1
hair_B
  rotate: false
  xy: 2, 412
  size: 266, 193
  orig: 266, 193
  offset: 0, 0
  index: -1
hair_F
  rotate: true
  xy: 583, 619
  size: 117, 98
  orig: 117, 98
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 607
  size: 317, 308
  orig: 317, 308
  offset: 0, 0
  index: -1
leg_B
  rotate: false
  xy: 678, 776
  size: 45, 113
  orig: 45, 113
  offset: 0, 0
  index: -1
leg_F
  rotate: true
  xy: 321, 420
  size: 40, 102
  orig: 40, 102
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 2, 2
  size: 69, 23
  orig: 69, 23
  offset: 0, 0
  index: -1
mouth_2
  rotate: false
  xy: 442, 450
  size: 76, 20
  orig: 76, 20
  offset: 0, 0
  index: -1
mouth_3
  rotate: true
  xy: 403, 236
  size: 90, 30
  orig: 90, 30
  offset: 0, 0
  index: -1
uparm_B
  rotate: true
  xy: 442, 472
  size: 52, 85
  orig: 52, 85
  offset: 0, 0
  index: -1
uparm_F
  rotate: false
  xy: 348, 236
  size: 53, 90
  orig: 53, 90
  offset: 0, 0
  index: -1
upleg_B
  rotate: true
  xy: 590, 561
  size: 56, 99
  orig: 56, 99
  offset: 0, 0
  index: -1
upleg_F
  rotate: true
  xy: 725, 808
  size: 59, 109
  orig: 59, 109
  offset: 0, 0
  index: -1
waist
  rotate: true
  xy: 348, 328
  size: 90, 77
  orig: 90, 77
  offset: 0, 0
  index: -1
weapon_01
  rotate: false
  xy: 2, 917
  size: 554, 105
  orig: 554, 105
  offset: 0, 0
  index: -1
