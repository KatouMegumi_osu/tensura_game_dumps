
sd_s_1088109.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_B
  rotate: true
  xy: 690, 21
  size: 57, 111
  orig: 57, 111
  offset: 0, 0
  index: -1
arm_F
  rotate: false
  xy: 772, 209
  size: 58, 111
  orig: 58, 111
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 587, 36
  size: 101, 153
  orig: 101, 153
  offset: 0, 0
  index: -1
brow_1
  rotate: false
  xy: 496, 4
  size: 72, 16
  orig: 72, 16
  offset: 0, 0
  index: -1
brow_2
  rotate: true
  xy: 996, 315
  size: 69, 16
  orig: 69, 16
  offset: 0, 0
  index: -1
brow_3
  rotate: true
  xy: 848, 102
  size: 67, 20
  orig: 67, 20
  offset: 0, 0
  index: -1
bubble_01
  rotate: false
  xy: 500, 191
  size: 178, 131
  orig: 178, 131
  offset: 0, 0
  index: -1
bubble_02
  rotate: false
  xy: 499, 324
  size: 185, 186
  orig: 185, 186
  offset: 0, 0
  index: -1
bubble_03
  rotate: false
  xy: 890, 386
  size: 124, 124
  orig: 124, 124
  offset: 0, 0
  index: -1
dead_b
  rotate: false
  xy: 686, 372
  size: 202, 138
  orig: 202, 138
  offset: 0, 0
  index: -1
dead_f
  rotate: false
  xy: 295, 183
  size: 203, 123
  orig: 203, 123
  offset: 0, 0
  index: -1
ef_grain_blue
  rotate: false
  xy: 690, 80
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
ef_particle_unatt_001b
  rotate: false
  xy: 279, 10
  size: 153, 171
  orig: 153, 171
  offset: 0, 0
  index: -1
ef_radical_straight
  rotate: false
  xy: 295, 308
  size: 202, 202
  orig: 202, 202
  offset: 0, 0
  index: -1
effect_bow
  rotate: true
  xy: 434, 22
  size: 159, 151
  orig: 159, 151
  offset: 0, 0
  index: -1
eye_1
  rotate: true
  xy: 832, 171
  size: 149, 37
  orig: 149, 37
  offset: 0, 0
  index: -1
eye_2
  rotate: true
  xy: 871, 214
  size: 156, 36
  orig: 156, 36
  offset: 0, 0
  index: -1
eye_3
  rotate: false
  xy: 686, 322
  size: 155, 48
  orig: 155, 48
  offset: 0, 0
  index: -1
hair
  rotate: false
  xy: 2, 6
  size: 275, 214
  orig: 275, 214
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 222
  size: 291, 288
  orig: 291, 288
  offset: 0, 0
  index: -1
leg_B
  rotate: false
  xy: 909, 281
  size: 42, 103
  orig: 42, 103
  offset: 0, 0
  index: -1
leg_F
  rotate: false
  xy: 953, 285
  size: 41, 99
  orig: 41, 99
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 587, 2
  size: 65, 32
  orig: 65, 32
  offset: 0, 0
  index: -1
mouth_2
  rotate: false
  xy: 434, 2
  size: 60, 18
  orig: 60, 18
  offset: 0, 0
  index: -1
mouth_3
  rotate: true
  xy: 850, 38
  size: 62, 23
  orig: 62, 23
  offset: 0, 0
  index: -1
nose
  rotate: false
  xy: 870, 108
  size: 56, 61
  orig: 56, 61
  offset: 0, 0
  index: -1
uparm_B
  rotate: false
  xy: 909, 194
  size: 46, 85
  orig: 46, 85
  offset: 0, 0
  index: -1
uparm_F
  rotate: false
  xy: 801, 92
  size: 45, 77
  orig: 45, 77
  offset: 0, 0
  index: -1
upleg_B
  rotate: false
  xy: 957, 204
  size: 49, 79
  orig: 49, 79
  offset: 0, 0
  index: -1
upleg_F
  rotate: false
  xy: 803, 20
  size: 45, 70
  orig: 45, 70
  offset: 0, 0
  index: -1
waist
  rotate: true
  xy: 680, 191
  size: 129, 90
  orig: 129, 90
  offset: 0, 0
  index: -1
waist_F_01
  rotate: false
  xy: 654, 6
  size: 31, 28
  orig: 31, 28
  offset: 0, 0
  index: -1
